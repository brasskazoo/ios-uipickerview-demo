//
//  BKViewController.h
//  PickerViewDemo
//
//  Created by Will Dampney on 7/02/2014.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BKViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

// Event methods
- (IBAction)beginEdit:(id)sender;
- (IBAction)endEdit:(id)sender;

@property (strong, nonatomic) NSArray *pickerValues;

@end
