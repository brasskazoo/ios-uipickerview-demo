//
//  BKViewController.m
//  PickerViewDemo
//
//  Created by Will Dampney on 7/02/2014.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import "BKViewController.h"

@interface BKViewController ()

@end

@implementation BKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Initialise data values
    _pickerValues = @[@"Moustache", @"Sandwich", @"Tractor"];

    // Create toolbar
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    
    // Create 'Done' button
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(pickerDoneClicked:)];
    [toolbar setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    // Assign picker, toolbar to text field
    [_textField setInputView:_pickerView];
    [_textField setInputAccessoryView:toolbar];
    
    // Assign delegate, data source of the UIPickerView
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ---- ---- ----
#pragma mark UIPickerView Datasource
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _pickerValues.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _pickerValues[row];
}

#pragma mark UIPickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    // Update textfield on selection change
    _textField.text = _pickerValues[row];
}

#pragma mark ---- ---- ----
#pragma mark Field Events
- (IBAction)beginEdit:(id)sender {
    NSLog(@"beginEdit:%@", sender);
    
    // Set textfield to the picker's default value if the field is empty
    //    This situation only occurs when the field is uninitialised (i.e. the first time it is edited).
    UITextField *textField = (UITextField *)sender;
    
    if ([textField.text isEqualToString:@""]) {
        NSInteger selected = [_pickerView selectedRowInComponent:0];
        NSLog(@"item %d selected: '%@'", selected, _pickerValues[selected]);

        textField.text = _pickerValues[selected];
    }
}

- (IBAction)endEdit:(id)sender {
    NSLog(@"endEdit:%@", sender);
}

#pragma mark ---- ---- ----
#pragma mark Button Events
- (void)pickerDoneClicked:(id)sender {
    NSLog(@"pickerDoneClicked:%@", sender);
    [_pickerView resignFirstResponder];
    
    NSInteger selected = [_pickerView selectedRowInComponent:0];
    NSLog(@"item %d selected: '%@'", selected, _pickerValues[selected]);
    
    // Update textfield to selected value (an alternative to the beginEdit: check)
//    _textField.text = _pickerValues[selected];
    
    [self.view endEditing:YES];
}
@end
