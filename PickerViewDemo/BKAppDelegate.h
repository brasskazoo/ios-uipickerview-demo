//
//  BKAppDelegate.h
//  PickerViewDemo
//
//  Created by Will Dampney on 7/02/2014.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
